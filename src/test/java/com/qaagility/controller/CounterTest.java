package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CounterTest {
    @Test
    public void testDigitalamdocs() throws Exception {

        int k1= new Counter().digitalamdocs(1,0);
        assertEquals("Digitalamdocs",Integer.MAX_VALUE, k1);
		
		int k2= new Counter().digitalamdocs(2,1);
        assertEquals("Digitalamdocs", 2, k2);
        
    }
}
